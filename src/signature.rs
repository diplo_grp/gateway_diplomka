use sha2::Sha512;
use ed25519_dalek::Signature;
use base58check::FromBase58Check;
use std::time::SystemTime;
use config::TZ_NODE;
use config::TIMESTAMP_VALIDATION;
use serde_json::Value;
use parser::Req;
use request_response;

#[derive(Debug,PartialEq)]
pub enum CheckSignatureError {
    GatewayError,
    SignatureDoesNotMatchError,
    TimestampIsNotValidError,
    OK,
}

pub fn verify_signature(public_key:String,message:String,signature:Signature) -> CheckSignatureError {
    let pk = public_key;
    let pk = str::replace(&pk, "\"", "");

    let pk = pk.from_base58check();
    let pk = match pk {
        Ok(pk) => pk,
        Err(error) => {
            warn!("failed to decode from base58check : {:?}", error);
            return CheckSignatureError::GatewayError
        }
    };
    let parsed_pk = &pk.1[3..];
    let public_key = ed25519_dalek::PublicKey::from_bytes(parsed_pk);
    let public_key = match public_key {
        Ok(public_key) => public_key,
        Err(error) => {
            warn!("failed to get ed25519 public key from bytes : {:?}", error);
            return CheckSignatureError::GatewayError
        },
    };
    let public_key = match pk.1.len() {
        35 => public_key,
        _ => {
            warn!("Key from Tezos RPC api does not have expected length");
            return CheckSignatureError::GatewayError;
        },
    };
    let signature_verifiaction = public_key.verify::<Sha512>(message.as_bytes(), &signature);
    if signature_verifiaction.is_ok()
    {
        return CheckSignatureError::OK;
    } else {
        return CheckSignatureError::SignatureDoesNotMatchError;
    }
}

pub fn check_signature_early_tx(dst_address1:String,signature:String) -> CheckSignatureError {
    info!("signature check started");
    let dst_address = str::replace(&dst_address1.to_string(),"\"","");
    let signed_json = dst_address.to_string();

    let str_json1 = serde_json::to_string(&signed_json);
    let str_json1 = match str_json1 {
        Ok(str_json1) => str_json1,
        Err(error) => {
            warn!("conversion from json to string failed : {:?}", error);
            return CheckSignatureError::GatewayError
        }
    };
    let str_json = str::replace(&str_json1, "\\", "");
    let str_json = format!("{}{}{}","{\"dst_address\":",str_json,"}");

    let signature1 = &signature;
    let signature = str::replace(&signature1, "\"", "");
    let signature = base64::decode(&signature);
    let signature = match signature {
        Ok(signature) => signature,
        Err(error) => {
            warn!("Decode from base64 error : {:?}", error);
            return CheckSignatureError::GatewayError
        }
    };
    let sign = ed25519_dalek::Signature::from_bytes(&signature);
    let sign = match sign {
        Ok(sign) => sign,
        Err(error) => {
            warn!("Signature retrieval from bytestream failed : {:?}", error);
            return CheckSignatureError::GatewayError
        }
    };
    //vytiahnutie public key cez tezos RPC
    let request = format!("http://{}:8732/chains/main/blocks/head/context/contracts/{}/manager_key", *TZ_NODE, dst_address);
    let response = request_response(request);

    let val = serde_json::from_str(&response);
    let val: Value = match val {
        Ok(val) => val,
        Err(error) => {
            warn!("failed to convert string to json : {:?}", error);
            return CheckSignatureError::GatewayError
        }
    };

    verify_signature(val["key"].to_string(),str_json,sign)
}

/*
*   Function to check if sender of txRequest is really owner of Tezos account
*   Sender has to provide proof by providing signed hash of prepared message
*   public key is base58check encoded
*/
pub fn check_signature(snd_address1:String,dst_address1:String,tx_value:i64,timestamp:u64,signature:String) -> CheckSignatureError {
    info!("signature check started");
    let dst_address = str::replace(&dst_address1.to_string(),"\"","");
    let snd_address = str::replace(&snd_address1.to_string(),"\"","");
    let signed_json = Req{dst_address:dst_address.to_string(),snd_address:snd_address.to_string(),tx_value:tx_value,timestamp:timestamp};
    let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();
    if now > timestamp + *TIMESTAMP_VALIDATION {
        return CheckSignatureError::TimestampIsNotValidError;
    }
    else {
        let str_json1 = serde_json::to_string(&signed_json);
        let str_json1 = match str_json1 {
            Ok(str_json1) => str_json1,
            Err(error) => {
                warn!("conversion from json to string failed : {:?}", error);
                return CheckSignatureError::GatewayError
            }
        };
        let str_json = str::replace(&str_json1, "\\", "");
        println!("{}",str_json);
        println!("{}",signature);
        let signature1 = &signature;
        let signature = str::replace(&signature1, "\"", "");
        let signature = base64::decode(&signature);
        let signature = match signature {
            Ok(signature) => signature,
            Err(error) => {
                warn!("Decode from base64 error : {:?}", error);
                return CheckSignatureError::GatewayError
            }
        };
        let sign = ed25519_dalek::Signature::from_bytes(&signature);
        let sign = match sign {
            Ok(sign) => sign,
            Err(error) => {
                warn!("Signature retrieval from bytestream failed : {:?}", error);
                return CheckSignatureError::GatewayError
            }
        };
        //vytiahnutie public key cez tezos RPC
        let request = format!("http://{}:8732/chains/main/blocks/head/context/contracts/{}/manager_key", *TZ_NODE, snd_address);
        let response = request_response(request);

        let val = serde_json::from_str(&response);
        let val: Value = match val {
            Ok(val) => val,
            Err(error) => {
                warn!("failed to convert string to json : {:?}", error);
                return CheckSignatureError::GatewayError
            }
        };
        verify_signature(val["key"].to_string(),str_json,sign)
    }

}