use std::fs::File;
use std::io::BufReader;
#[derive(Deserialize,Debug)]
pub struct Config {
    pub contract_address:String,
    pub contract_nickname:String,
    pub user_nickname:String,
    pub tezos_client_path:String,
    pub tezos_node_ip:String,
    pub ssh_address:String,
    pub timestamp_validation_period:u64,
    pub execution_period:u64,
    pub forced_tx_fee:i64,
    pub mini_batch_parallel_execution:bool,
    pub batch_execution:bool,
    pub mini_batch_execution:bool,
}
lazy_static! {
pub static ref PATH:String = {
let path = "../../config.json".to_string();
path
};
}
 lazy_static!{
    pub static ref CNT_ADDR:String = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let cnt_addr = config.contract_address;
    cnt_addr
    };
    pub static ref CNT_NICK:String = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let cnt_nick = config.contract_nickname;
    cnt_nick
    };
    pub  static ref USR_NICK:String = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let usr_nick = config.user_nickname;
    usr_nick
    };
    pub  static ref TZ_CLIENT_PATH:String = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let tz_client_path = config.tezos_client_path;
    tz_client_path
    };
    pub static ref TZ_NODE:String = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let tz_node = config.tezos_node_ip;
    tz_node
    };
    pub static ref SSH_ADDRESS:String = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let ssh_address = config.ssh_address;
    ssh_address
    };
    pub static ref TIMESTAMP_VALIDATION:u64 = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let timestamp_validation = config.timestamp_validation_period;
    timestamp_validation
    };
    pub static ref EXECUTION_PERIOD:u64 = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let execution_period = config.execution_period;
    execution_period
    };
    pub static ref FORCED_TX_FEE:i64 = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let fee = config.forced_tx_fee;
    fee
    };
    pub static ref MINI_BATCH_PARALLEL_EXECUTION:bool = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let mini_batch_parallel_execution = config.mini_batch_parallel_execution;
    mini_batch_parallel_execution
    };
    pub static ref BATCH_EXECUTION:bool = {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let batch_execution = config.batch_execution;
    batch_execution
    };
    pub static ref MINI_BATCH_EXECUTION:bool= {
    let path = &*PATH;
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let config:Config = serde_json::from_reader(reader).unwrap();
    let mini_batch_execution = config.mini_batch_execution;
    mini_batch_execution
    };
}
