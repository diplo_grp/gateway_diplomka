#![feature(proc_macro_hygiene, decl_macro)]
#![feature(custom_attribute)]

//crypto
extern crate base64;
extern crate sha2;
extern crate ed25519_dalek;
extern crate base58check;
//shell crate
#[macro_use]
extern crate shells;

//rest server+httprequests
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
extern crate rocket_cors;
extern crate rocket_client_addr;
extern crate reqwest;
extern crate url;

//json
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate lazy_static;

//timer crates
extern crate futures;
extern crate futures_timer;

//logger
#[macro_use]
extern crate log;
extern crate core;


use std::thread;
use std::time::Duration;
use url::Url;
use rocket::State;
use rocket_client_addr::ClientAddr;
use rocket_contrib::json::Json;
use std::sync::Mutex;
use log::{warn,info};
mod parser;
use parser::{SignedReq};
use parser::{SignedReqTx};
use parser::{parse_storage_json};
use parser::{ParseJsonError};
mod config;
use config::*;
mod signature;
use signature::*;

type RequestApprovedVec = Mutex<Vec<SignedReq>>;


#[post("/early_tx" ,data = "<req>")]
fn check_request_early_tx(req: Json<SignedReqTx>, vec: State<RequestApprovedVec>) -> String {
    info!("Processing of early transaction payment request started");
    //check if request is validly signed

    let log = check_signature_early_tx(req.0.dst_address.to_string(),req.0.signature.to_string());
    info!("Signature check - Done");

    if log == CheckSignatureError::GatewayError {
        warn!("check_signature result = Gateway Error\n");
        "Error is on our side\n".to_string()
    }
    else if log == CheckSignatureError::SignatureDoesNotMatchError {
        warn!("check_signature result = Mismatched Signature error\n");
        "Signature is not valid\n".to_string()
    }
    else if log == CheckSignatureError::TimestampIsNotValidError {
        warn!("check_signature result = Timestamp too old\n");
        "Timestamp too old\n".to_string()
    }
    else {
        info!("check_signature result = OK");
        //get contract storage
        let request = format!("http://{}:8732/chains/main/blocks/head/context/contracts/{}/storage",*TZ_NODE,*CNT_ADDR);
        let response = request_response(request);
        //find how much tez has user stored in contract
        let val = parse_storage_json(response, req.0.dst_address.to_string());
        if val == ParseJsonError::ZeroBalance{
            warn!("Sender has not inserted tez into smart contract");
            return "You haven't any balance stored in smart contract\n".to_string()
        }
        else if val == ParseJsonError::InvalidJsonError{
            return "Cannot accept tx request right now\n".to_string()
        }
        else
        {  let _val = match val {
            ParseJsonError::OK(val) => val,
            ParseJsonError::ZeroBalance => return "You haven't any balance stored in smart contract\n".to_string(),
            ParseJsonError::InvalidJsonError => {warn!("Sender has not inserted tez into smart contract"); return "Cannot accept tx request right now\n".to_string()}

        };
            let mut accepted = false;
            let mut ind:usize = 0;
            let mut new_rec:SignedReq = SignedReq{ snd_address: "NULL".to_string(),  dst_address:"NULL".to_string(),tx_value: 0, timestamp: 0, signature: "NULL".to_string(),};
            //prehladanie schvalenych transakcii od pouzivatela s danou adresou
            let mut vec = vec.lock().expect(" lock.");
            // spocitanie jeho doterajsich schvalenych transakcii + pozretie ulozeneho zostatku v contracte a porovnanie - nasledne vyhodnotenie
            for (index,item) in vec.iter().enumerate() {
                if item.dst_address == req.0.dst_address && item.tx_value - *FORCED_TX_FEE >= 0{
                     new_rec = SignedReq{
                        snd_address: item.snd_address.clone(),
                        dst_address: item.dst_address.clone(),
                        tx_value: item.tx_value.clone() - *FORCED_TX_FEE,
                        timestamp: item.timestamp.clone(),
                        signature: item.signature.clone(),
                    };
                    ind = index;
                    accepted = true;
                    break;
                }
            }

            if !accepted {
                return "You don't have enough tez stored in contract to force early tx payment\n".to_string();
            }
            vec.remove(ind);
            vec.push(new_rec);
            //lock storage for accepted requests
            info!("Transaction request processing - Done");
            std::mem::drop(vec);
            settle_tx();
            "Your request was accepted\n".to_string()

        }
    }
}

fn request_response(request:String) -> String {
    let request_url = Url::parse(&request);
    let request_url = match request_url {
        Ok(request_url) => request_url,
        Err(error) => {warn!("failed to create url from string : {:?}",error); return "Error is on our side\n".to_string()}
    };
    let response = reqwest::get(request_url);
    let mut response  = match response {
        Ok(response) => response,
        Err(error) => {warn!("failed to get response from tezos API : {:?}",error);return "Error is on our side\n".to_string()}
    };
    let response  = response.text();
    match response {
        Ok(response) => response,
        Err(error) => {warn!("failed to convert response to string : {:?}",error);return "Error is on our side\n".to_string()}
    }
}
// po requeste zistit ci ma odosielatel vlozenych dost penazi na ucte(peniaze na ucte - vsetky nevybavene transakcie <= ako stav uctu), ak ano potrvdit platbu, ak nie tak zahodit
#[post("/" ,data = "<req>")]
fn check_request(req: Json<SignedReq>, vec: State<RequestApprovedVec>) -> String {
    info!("Processing of transaction request started");
    //check if request is validly signed

    let log = check_signature(req.0.snd_address.to_string(), req.0.dst_address.to_string(), req.0.tx_value, req.0.timestamp,req.0.signature.to_string());
    info!("Signature check - Done");

    if log == CheckSignatureError::GatewayError {
        warn!("check_signature result = Gateway Error\n");
        "Error is on our side\n".to_string()
    }
    else if log == CheckSignatureError::SignatureDoesNotMatchError {
        warn!("check_signature result = Mismatched Signature error\n");
        "Signature is not valid\n".to_string()
    }
    else if log == CheckSignatureError::TimestampIsNotValidError {
        warn!("check_signature result = Timestamp too old\n");
        "Timestamp too old\n".to_string()
    }
    else {
        info!("check_signature result = OK");
        //get contract storage
        let request = format!("http://{}:8732/chains/main/blocks/head/context/contracts/{}/storage",*TZ_NODE,*CNT_ADDR);
        let response = request_response(request);
        //find how much tez has user stored in contract
        let val = parse_storage_json(response, req.0.snd_address.to_string());
        if val == ParseJsonError::ZeroBalance{
            warn!("Sender has not inserted tez into smart contract");
            return "You haven't any balance stored in smart contract\n".to_string()
        }
        else if val == ParseJsonError::InvalidJsonError{
            return "Cannot accept tx request right now\n".to_string()
        }
        else
         {  let val = match val {
            ParseJsonError::OK(val) => val,
            ParseJsonError::ZeroBalance => return "You haven't any balance stored in smart contract\n".to_string(),
            ParseJsonError::InvalidJsonError => {warn!("Sender has not inserted tez into smart contract"); return "Cannot accept tx request right now\n".to_string()}

                };
            //prehladanie schvalenych transakcii od pouzivatela s danou adresou
            let mut vec = vec.lock().expect(" lock.");
            // spocitanie jeho doterajsich schvalenych transakcii + pozretie ulozeneho zostatku v contracte a porovnanie - nasledne vyhodnotenie
            let mut accepted_tx_val: i64 = req.0.tx_value;
            for i in vec.iter() {
                if i.snd_address == req.0.snd_address {
                    accepted_tx_val = accepted_tx_val + i.tx_value;
                }
            }
            if val.parse::<i64>().unwrap() < accepted_tx_val {
                return "You don't have enough tez stored in contract\n".to_string();
            }
            //lock storage for accepted requests
            vec.push(req.0);
            info!("Transaction request processing - Done");
             println!("vec len {}",vec.len());
            "Your request was accepted\n".to_string()

        }
    }
}

//: citanie ziskaneho vektora
#[get("/")]
fn read_request(vec: State<RequestApprovedVec>) -> () {

    let vec = vec.lock().expect(" lock.");
    for r in vec.iter() {
     //   if r.snd_address == address {
            println!("snd_address: {} dst_address: {}  tx:value{}", r.snd_address, r.dst_address, r.tx_value);
     //   }
    }
}

#[get("/tx")]
fn do_tx (client_addr: ClientAddr,vec :State<RequestApprovedVec>) -> String {
    //invoke finalize method of smart contract
    let sender_addr = client_addr.get_ipv4_string().unwrap();
    if sender_addr != "127.0.0.1" {
        return "YOU CANT SETTLE TX".to_string()
    }
    else {
            println!(" address of sender is : {}", sender_addr);
            let mut vec = vec.lock().expect(" lock.");
        let vec1 = vec.clone();
        vec.clear();
        std::mem::drop(vec);
        if *MINI_BATCH_EXECUTION  {

            let mut cnt:u8 = 1;
            let mut tx_call = "".to_string();
            let  iter = vec1.iter().peekable();
            for i in iter {
                //break batch into 13 TXs

                if cnt % 12 == 1{
                     tx_call = format!("{} transfer 0 from {} to {} -arg '(Pair(Right(Right(Left Unit))) (Pair 2  {{", *TZ_CLIENT_PATH, *USR_NICK, *CNT_NICK);
                }
                tx_call = format!("{} Pair \"{}\" (Pair \"{}\" {});", tx_call, &i.snd_address, &i.dst_address, &i.tx_value.to_string());

                if cnt % 12 == 0 || cnt == vec1.len()as u8 {
                        tx_call.push_str("}))' --burn-cap 100 --force-low-fee \n");
                        if *SSH_ADDRESS != ""
                        {
                            tx_call = tx_call.replace("\"", "\\\"");
                            tx_call = format!("{} \" {}\"", *SSH_ADDRESS, tx_call);
                        }
                        let res = wrap_bash!("{}",tx_call);
                        match res {
                            Ok(k) => {
                                info!("bash executed successfully {:?}", k);
                            },
                            Err(e) => {
                                warn!("error while executing shell command {:?}", e);
                            }
                        }

            }
                println!{"{}",cnt};
                cnt = cnt + 1;
            }
        };
        if *BATCH_EXECUTION {
            let mut tx_call: String = format!("{} transfer 0 from {} to {} -arg '(Pair(Right(Right(Left Unit))) (Pair 2  {{", *TZ_CLIENT_PATH, *USR_NICK, *CNT_NICK);
            for i in vec1.iter() {
                tx_call = format!("{} Pair \"{}\" (Pair \"{}\" {});", tx_call, &i.snd_address, &i.dst_address, &i.tx_value.to_string());
            }
            tx_call.push_str("}))' --burn-cap 100 --force-low-fee \n");

            // ####use if deployed trough docker####
            if *SSH_ADDRESS != ""
            {
                tx_call = tx_call.replace("\"", "\\\"");
                tx_call = format!("{} \" {}\"", *SSH_ADDRESS, tx_call);
            }
            let res = wrap_bash!("{}",tx_call);
            match res {
                Ok(k) => {
                    info!("bash executed successfully {:?}", k);
                },
                Err(e) => {
                    warn!("error while executing shell command {:?}", e);
                }
            };
        }
     
        if *MINI_BATCH_PARALLEL_EXECUTION {
            let mut cnt:u8 = 1;
            let mut tx_call = "".to_string();
            let  iter = vec1.iter().peekable();
            for i in iter {
                //break batch into 13 TXs

                if cnt % 12 == 1{
                    tx_call = format!("{} transfer 0 from {} to {} -arg '(Pair(Right(Right(Left Unit))) (Pair 2  {{", *TZ_CLIENT_PATH, *USR_NICK, *CNT_NICK);
                }
                tx_call = format!("{} Pair \"{}\" (Pair \"{}\" {});", tx_call, &i.snd_address, &i.dst_address, &i.tx_value.to_string());

                if cnt % 12 == 0 || cnt == vec1.len()as u8 {
                    tx_call.push_str("}))' --burn-cap 100 --force-low-fee \n");
                    if *SSH_ADDRESS != ""
                    {
                        tx_call = tx_call.replace("\"", "\\\"");
                        tx_call = format!("{} \" {}\"", *SSH_ADDRESS, tx_call);
                    }
                    let tx_call_parallel = tx_call.clone();
                    thread::spawn( move || {let res = wrap_bash!("{}",tx_call_parallel);
                        match res {
                            Ok(k) => {
                                info!("bash executed successfully {:?}", k);
                            },
                            Err(e) => {
                                warn!("error while executing shell command {:?}", e);
                            }
                        } });

                }
                println!{"{}",cnt};
                cnt = cnt + 1;
            }
        };

    }
    "OK".to_string()
}

fn settle_tx (){
    let response = reqwest::get("http://localhost:8000/request/tx");

    println!("{:?}",response);
}

fn main()  {

    //let dur = Duration::from_secs(EXECUTION_PERIOD);
    //let child = thread::spawn(move || {
    //    loop {
     //       thread::sleep(dur);
     //       settleTx();
    //    }
    //});
    let default = rocket_cors::Cors::default();
    rocket::ignite()
       //.catch(errors![internal_error, not_found])
        .mount("/request", routes![check_request,read_request,do_tx,check_request_early_tx])
        .attach(default)
        .manage( Mutex::new(Vec::<SignedReq>::new()))
        .launch();
}

