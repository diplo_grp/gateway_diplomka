


#[derive(Serialize, Deserialize,Debug,Clone)]
pub struct Req {
    pub dst_address:String,
    pub snd_address:String,
    pub tx_value:i64,
    pub timestamp: u64,
}

#[derive(Serialize, Deserialize,Debug,Clone)]
pub struct SignedReq {
    pub dst_address:String,
    pub snd_address:String,
    pub tx_value:i64,
    pub timestamp:u64,
    pub signature:String,
}

#[derive(Serialize, Deserialize,Debug,Clone)]
pub struct SignedReqTx {
    pub dst_address:String,
    pub signature:String,
}
#[derive(Serialize, Deserialize,Debug)]
enum Prim {
    Pair,
    Elt,
    Some,
    None
}
#[derive(Deserialize, Debug)]
struct Node {
    #[serde(flatten)]
    pub variant: Variant,
    #[serde(default)]
    pub args: Vec<Nodes>,
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum Nodes {
    Single(Node),
    Multiple(Vec<Node>),
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
enum Variant {
    Prim(Prim),
    Int(String),
    String(String),
}
#[derive(Debug,PartialEq)]
pub enum ParseJsonError {
    InvalidJsonError,
    ZeroBalance,
    OK(String),
}
// pokus o najdenie zostatku pouizivatela s adresou X z jsonu vrateneho Tezos API
pub fn parse_storage_json(json:String,address: String) -> ParseJsonError  {

    let json_deserialized:Result<Node,serde_json::Error> = serde_json::from_str(&json);
    let json_deserialized = match json_deserialized {
        Ok(json_deserialized) => json_deserialized,
        Err(error) => {warn!("failed to serialize json/either Tezos api changed or serialization isn't correctly implemented : {:?}",error); return ParseJsonError::InvalidJsonError}
    };
    let nodes: Vec<Nodes> = json_deserialized.args;
    //gateway's part of storage
    let gtw = &nodes[0];
    // rozbalenie jsonu az po adresu
    //ak najdeme adresu ktoru hladame vratime jej zostavajuci balance
    if let Nodes::Single(ref s) = *gtw  {
        for i in s.args.iter(){
            if let Nodes::Multiple(ref s) = *i {
                for j in s.iter() {
                    for k in j.args.iter(){
                        if let Nodes::Single(ref s) = *k {
                            if let Variant::String(ref addr) = s.variant{
                                if *addr == address {
                                    if let Nodes::Single(ref s)  = j.args[1]{
                                        if let Variant::Int(ref bal) = s.variant {
                                            let ret = bal.to_owned();
                                            return ParseJsonError::OK(ret.to_string());
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    return ParseJsonError::ZeroBalance;
}