# Gateway pre kryptomenu Tezos
Repozitár obsahuje zdrojopvé kódy k diplomovej práci 

## Popis adresárov a súborov

**examples** - obsahuje príklad valídnej žiadosti a shell príkazy, ktorými sa dá vykonať transakcia on-chain

**tezos** - obsahuje zdrojový kód smart contractu v jazyku Liquidity a taktiež "skompilovaný" zdrojový kód v jazyku Michelson

**src** - obsahuje zdrojový kód k off-chain komponentu 

**html** - obsahuje zdrojový kód k front-endu 

**Cargo.toml** - obsahuje balíčky na ktorých je zdrojový kód off-chain časti závislí

**Dockerfile** - Dockerfile na vytvorenie kontajneru s off-chain komponentom platobnej brány a front-endom

## Inštalácia

Pre správny chod platobnej brány je potebné mať zosynchronizovaný Tezos blockchain

Príkazy na vytvorenie smart contractu sa nachádzajú v skripte examples/deployscript.sh

Najjednoduchší spôsob ako spustiť off-chain komponent je s využitím Docker kontajneru.
Po spustení vytvorení a spustení kontajneru je potrebné sa do ňho pripojiť a manuálne spustiť apache web server a off-chain komponent. 
