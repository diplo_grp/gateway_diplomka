FROM rustlang/rust:nightly
RUN apt-get update  && apt-get upgrade -y && apt-get install -y apache2 git openssh-client 
RUN rustup default nightly-2019-02-10
RUN git clone -b master https://gitlab.com/diplo_grp/gateway_diplomka
RUN cd gateway_diplomka && cp -R html /var/www/
RUN cd gateway_diplomka && cargo build --release
#RUN rm -rf /var/lib/apt/lists/*


EXPOSE 80
ExPOSE 8000
CMD ["apachectl", "-D", "FOREGROUND"]
#CMD cd gateway_diplomka && ./init.sh

