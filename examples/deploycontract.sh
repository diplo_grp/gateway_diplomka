#!/bin/sh
#nickname for contract
CONTRACT=gateway
#Tezos address of contract
CONTRACT_ADDR=x
#Tezos user nickname
USER=test
#compile liquidity into michelson
liquidity $CONTRACT.liq
#deploy contract
./tezos-client originate contract $CONTRACT for $USER transferring 2.01 from $USER running $CONTRACT.liq.tz -init '(Pair(None)(None))' --burn-cap 100 --force-low-fee
# initiate gateway - 2tz to gateway reserve
tezos-client transfer 2 from $USER to test -arg '(Pair(Left Unit) (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair 2 {Pair "KT1GE2AZhazRxGsAjRVkQccHcB2pvANXQWd7" (Pair "KT1GE2AZhazRxGsAjRVkQccHcB2pvANXQWd7" 0) ; Pair "KT1GE2AZhazRxGsAjRVkQccHcB2pvANXQWd7" (Pair "KT1GE2AZhazRxGsAjRVkQccHcB2pvANXQWd7" 1000000);})))' --burn-cap 100 --force-low-fee
# add funds as a sender
tezos-client transfer 1 from $USER to test -arg '(Pair(Right(Left Unit)) (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair 2 {Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" 0) ; Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" 1000000);})))' --burn-cap 100 --force-low-fee
# execute tx on-chain
tezos-client transfer 0 from $USER to test -arg '(Pair(Right(Right(Left Unit))) (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair 2 {Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" 1000000) ; Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" (Pair "tz1bxbCzwSoqpAKXV6w54Y9Ta4uEasZdr1FM" 1000000);})))' --burn-cap 100 --force-low-fee
## rpc call to get tezos storage
curl http://127.0.0.1:8732/chains/main/blocks/head/context/contracts/$CONTRACT_ADDR/storage
## send transaction request to gateway
curl -H "Content-Type: application/json" -d @test.json  -X POST http://localhost:8000/request
